/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dato;

import javax.swing.JOptionPane;

public class Datos {
    private String nombre;
    private String carrera;
    private Integer edad;
    
    
    public Datos(String nombre, String carrera, Integer edad){
        this.nombre = nombre;
        this.carrera = carrera;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String Carrera) {
        this.carrera = Carrera;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    
    public Object[] enviarDatos(String nombre, String carrera, Integer edad){
        Object[] estudiante = {nombre, carrera, edad};
        return estudiante;
        
    }
    
    public boolean validarDatos(String nombre, String carrera, Integer edad){
        if(this.nombre.equals("") || this.carrera.equals("")){
            JOptionPane.showMessageDialog(null, "Complete el formulario");
            return false;
        }
        if(edad <= 0 || edad >= 100){
            JOptionPane.showMessageDialog(null, "Introduzca una edad válida");
            return false;
        }
        return true;
    }
}
